//
//  ngmLoginTextField.swift
//  Instagram
//
//  Created by Nicholas Chung on 2/24/17.
//  Copyright © 2017 Nicholas Chung. All rights reserved.
//

import UIKit

class ngmLoginTextField: UITextField {

    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Border
        self.layer.cornerRadius = 5.0
        self.clipsToBounds = true
    }
}
